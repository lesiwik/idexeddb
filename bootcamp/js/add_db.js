var Database = (function(){
	var databaseObject = {};
	databaseObject.bobDylanPesel = '80050313334';

	var dummyData = {
		clients: [
			{
				pesel: databaseObject.bobDylanPesel,
				name: 'Bob Dylan',
				year: '1941'
			},
			{
				pesel: '90010112345',
				name: 'Elvis Presley',
				year: '1930'
			},
			{
				pesel: '82050313334',
				name: 'Diana Ross',
				year: '1940'
			}
		],
		carModels: [
			{
				manufacturer: 'Audi',
				model: 'A4',
			},
			{
				manufacturer: 'Volvo',
				model: 'S60'
			}
		],
		carItems: [
			{
				plate: 'ABC787',
				modelId: 'Audi A4',
				clientId: '80050313334'
			},
			{
				plate: 'BCD876',
				modelId: 'Audi A4',
				clientId: null
			}
		]
	}

	function NotImplementedYetException(message){
		this.message = message;
	}

	function notImplemented(functionName){
		var message = "Function " + functionName + " not implemented yet.";
		console.log(message);
		throw new NotImplementedYetException(message);
	}

	function fixMe(){
		throw new NotImplementedYetException("You forgot to fix something");
	}

	var connection;
	var dbName;
	var dbVersion;

	databaseObject.connect = function(dbName, dbVersion, onupgradeneededCallback, onsuccessCallback) {
		// notImplemented("connect");

		console.log(connection);
		if(connection && dbVersion == connection.version && dbName == connection.name) {
			console.log("You are already connected");
			return;
		}

		var request = indexedDB.open(dbName, dbVersion);
		updateDbNameAndVersion(dbName, dbVersion);
		// var request = indexedDB.open(fixMe(), fixMe());
		

		request.onerror = function(event) {
			console.log(event);
			console.log('Napotkano błąd podczas połączenia: <code>' + event.target.error.message + '</code>', 'danger');
		};

		request.onsuccess = function(ev) {
			connection = request.result;
			console.log('Połączony do bazy <code>' + connection.name + '</code> (ver. ' + connection.version + ')', 'success');

			// read more about error handling and error bubbling: https://developer.mozilla.org/pl/docs/IndexedDB/Using_IndexedDB#Handling_Errors
			connection.onerror = function(event){
				console.log("Database error: " + event.target.errorCode);
			}

			connection.onversionchange = function(event) {
				console.log(event);
				console.log('Pojawiła się nowa wersja! Rozłącz się!', 'danger');
			};

			if(onsuccessCallback){
				onsuccessCallback(ev);
			}
		};

		request.onblocked = function(event) {
			console.log(event);
			console.log('Połączenie zablokowane. Zresetuj swoje połączenie na innych kartach!', 'danger');
		};

		request.onupgradeneeded = function(event) {
			if(onupgradeneededCallback){
				connection = request.result;
				onupgradeneededCallback(event);
			}
		}

		// request.onupgradeneeded = function() {
		// 	connection = request.result;
		// 	if (!connection.objectStoreNames.contains(dataStore)) {
		// 		var users = connection.createObjectStore(dataStore, {
		// 			keyPath: 'email'
		// 		});
		// 		var req = users.add({
		// 			email: 'bob@dylan.com',
		// 			name: 'Bob Dylan',
		// 			text: 'I am a great musician ;)'
		// 		});
		// 		console.log('Utworzono data store <b><code>users</code></b> i umieszczono przykładowe dane');
		// 	}
		// }
	};

	function updateDatabaseSchema(updatingSchemaFunction, onsuccessCallback){
		// notImplemented('updateDatabaseSchema');
		if(!connection){
			console.log("updateDatabaseSchema expects to have established connection");
			throw "updateDatabaseSchema expects to have established connection";
		}

		var newVersion = connection.version + 1;
		// var newVersion = fixMe();
		var name = connection.name;
		connection.close();

		databaseObject.connect(name, newVersion, updatingSchemaFunction, onsuccessCallback);
		console.log(newVersion);
		updateDbNameAndVersion(name, newVersion);
		// databaseObject.connect(fixMe(), fixMe(), fixMe());
	}

	databaseObject.createDataStoreClients = function(){
		// notImplemented('createDataStoreClient');

		updateDatabaseSchema(function(){
			var clients = connection.createObjectStore('clients', {
				keyPath: 'pesel'
			});	
		});

		// takie powinno byc pierwsze podejscie, ale nie zadziala, bo createDataStore nie jest wywolywany wewnatrz version change transaction
		// potrzebne jest skorzystanie z updateDatabaseSchema, ktory zadba o to, zeby kod byl wywolany wewnatrz version change transaction
		// var clients = connection.createObjectStore('clients', {
		// 	keyPath: 'pesel'
		// });

		// var clients = connection.createObjectStore(fixMe(), fixMe());
	}

	databaseObject.createDataStoreCarModels = function(){
		// notImplemented('createDataStoreCarModels');

		updateDatabaseSchema(function(){
			var models = connection.createObjectStore('carModels', {
				keyPath: 'modelId',
				autoIncrement: true
			});
		});

		// pierwsze podejsce
		// var models = connection.createObjectStore('carModels', {
		// 	keyPath: 'modelId',
		// 	autoIncrement: true
		// });

		// var models = connection.createObjectStore(fixMe(), fixMe());
	}

	databaseObject.createDataStoreCarItems = function(){
		// notImplemented('createDataStoreCarItems');

		updateDatabaseSchema(function(){
			var items = connection.createObjectStore('carItems', {
				autoIncrement: true
			});
		})

		// pierwsze podejscie
		// var items = connection.createObjectStore('carItems', {
		// 	autoIncrement: true
		// });
		// var items = connection.createObjectStore(fixMe(), fixMe());
	}

	databaseObject.insertClientRows = function(){
		// notImplemented('insertClientRows');

		var transaction = connection.transaction('clients', 'readwrite');

		var data = dummyData['clients'];
		for(var i = 0; i<data.length; ++i){
			var req = transaction.objectStore('clients').add(data[i]);

			req.onsuccess = function(){};
			req.onerror = function(){};	
		}
	}

	function generateCarModelId(carModelObject){
		// notImplemented('generateCarModelId');
		return carModelObject.manufacturer + ' ' + carModelObject.model;
	}

	databaseObject.insertCarModelRows = function(){
		// notImplemented('insertCarModelRows');

		var transaction = connection.transaction('carModels', 'readwrite');

		var data = dummyData['carModels'];
		for(var i = 0; i<data.length; ++i){
			var toInsert = data[i];
			toInsert.modelId = generateCarModelId(data[i]);
			var req = transaction.objectStore('carModels').add(toInsert);

			req.onsuccess = function(){};
			req.onerror = function(){};	
		}
	}

	databaseObject.insertCarItemRows = function(){
		// notImplemented('insertCarModelRows');

		var transaction = connection.transaction('carItems', 'readwrite');

		var data = dummyData['carItems'];
		for(var i = 0; i<data.length; ++i){
			var req = transaction.objectStore('carItems').add(data[i]);

			req.onsuccess = function(){};
			req.onerror = function(){};	
		}
	}

	databaseObject.getClientByPesel = function(pesel){
		//notImplemented('getClientByPesel');

		var transaction = connection.transaction('clients');
		var objectStore = transaction.objectStore('clients');
		var req = objectStore.get(pesel);

		req.onsuccess = function(event) {
			if(req.result){
				console.log('Successfully fetched user by pesel ' + pesel);
				var user = req.result;
				console.log(user);	
			}else{
				console.log('Cannot fetch user by pesel ' + pesel);
			}
		}
	}

	databaseObject.getAllCarItems = function(){
		// notImplemented('getAllCarItems');

		var transaction = connection.transaction('carItems');
		var objectStore = transaction.objectStore('carItems');

		var items = [];
		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;
			if (cursor) {
				var key = cursor.key;
				var value = cursor.value;
				items.push({key: key, value: value});
		    	cursor.continue();
		  	}else {
		    	console.log(items);
		  	}
		};
	};

	databaseObject.createClientNameIndex = function(){
		createIndex('clients', 'name', 'name', true);
	}

	function getObjectStore(dataStoreName, mode){
		var transaction;
		if(mode)
			transaction = connection.transaction(dataStoreName, mode);
		else
			transaction = connection.transaction(dataStoreName)

		var objectStore = transaction.objectStore(dataStoreName);
		return objectStore;
	}

	databaseObject.findClient = function(name, onsuccessCallback){
		// notImplemented('findClient');
		var transaction = connection.transaction('clients');
		var objectStore = transaction.objectStore('clients');
		var index = objectStore.index('name');
		var req = index.get(name);

		req.onsuccess = function(event){
			if(req.result){
				console.log('Successfully fetched client with name ' + name);
				console.log(event.target.result);	
				if(onsuccessCallback){
					onsuccessCallback(event.target.result);
				}
			}else{
				console.log('Cannot fetch client with name ' + name);
			}
		}
	};

	databaseObject.createClientIdIndex = function(){
		createIndex('carItems', 'clientIdIndex', 'clientId', false);
	};

	function createIndex(objectStoreName, indexName, fieldName, unique){
		var objectStore = getObjectStore(objectStoreName);
		if(objectStore.indexNames.contains(indexName)){
			console.log("Index" + indexName + " already exists.");
		}else{
			updateDatabaseSchema(function(event){
				var objectStore = event.currentTarget.transaction.objectStore(objectStoreName);
				objectStore.createIndex(indexName, fieldName, { unique: unique });
				console.log("Index " + indexName + " created successfully.");
			});
		}
	}

	databaseObject.createYearIndex = function(){
		createIndex('clients', 'yearIndex', 'year', false);
	}

	databaseObject.findCarsForUsers = function(pesel, onsuccessCallback){
		var objectStore = getObjectStore('carItems');
		var index = objectStore.index('clientIdIndex');

		var singleKeyRange = IDBKeyRange.only(pesel);
		var cars = [];

		index.openCursor(singleKeyRange).onsuccess = function(event){
			var cursor = event.target.result;
			if(cursor){
				cars.push(cursor.value);
				cursor.continue();
			}else{
				console.log("There are " + cars.length + " car for user " + pesel);
				console.log(cars);
				if(onsuccessCallback)
					onsuccessCallback(cars);
			}
		}
	};

	databaseObject.findBornBefore = function(year){
		var objectStore = getObjectStore('clients');
		var index = objectStore.index('yearIndex');
		var range = IDBKeyRange.upperBound(year, false);

		var users = [];

		index.openCursor(range).onsuccess = function(event){
			var cursor = event.target.result;
			if(cursor){
				users.push(cursor.value);
				cursor.continue();
			}else{
				console.log(users);
			}
		}
	};

	databaseObject.changeClientYear = function(pesel, newYear){
		var objectStore = getObjectStore('clients', 'readwrite');
		req = objectStore.get(pesel);

		req.onsuccess = function(event){
			var client = req.result;
			client.year = newYear;

			var requestUpdate = objectStore.put(client);
		   	requestUpdate.onsuccess = function(event) {
		    	console.log("Client's year has been updated");
		   	};
		}
	};

	databaseObject.deleteUserByName = function(name, consistencyCheck){
		databaseObject.findClient(name, function(user){
			deleteUserByPesel(user.pesel);
		});

		function deleteUserByPesel(pesel){
			if(consistencyCheck){
				databaseObject.findCarsForUsers(pesel, function(cars){
					if(cars.length == 0){
						deleteUser(pesel);
					}else{
						console.log("Cannot delete user that has some cars");
					}
				});
			}else{
				deleteUser(pesel);
			}
		}
		
		function deleteUser(pesel){
			var objectStore = getObjectStore('clients', 'readwrite');
			var req = objectStore.delete(pesel);

			req.onsuccess = function(event){
				console.log("User " + name + " has been deleted.");
			}
		}
	};

	databaseObject.addCity = function(){
		var transaction = connection.transaction('clients', 'readwrite');
		var objectStore = transaction.objectStore('clients');

		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;
			if (cursor) {
				var client = cursor.value;
				client.city = 'Katowice';

				// we are not interested in result
				objectStore.put(client);
		    	cursor.continue();
		  	}else {
		    	console.log("adding cities finished");
		  	}
		};
	};

	return databaseObject;
})();