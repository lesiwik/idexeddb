var Database = (function(){
	var Constants = {
		clients: 'Clients',
		models: 'CarModels',
		items: 'CarItems'
	};
	var databaseObject = {};
	databaseObject.bobDylanPesel = '80050313334';

	var dummyData = {
		Clients: [
			{
				pesel: databaseObject.bobDylanPesel,
				name: 'Bob Dylan',
				year: '1941'
			},
			{
				pesel: '90010112345',
				name: 'Elvis Presley',
				year: '1930'
			},
			{
				pesel: '82050313334',
				name: 'Diana Ross',
				year: '1940'
			}
		],
		CarModels: [
			{
				manufacturer: 'Audi',
				model: 'A4',
			},
			{
				manufacturer: 'Volvo',
				model: 'S60'
			}
		],
		CarItems: [
			{
				plate: 'ABC787',
				modelId: 'Audi A4',
				clientId: '80050313334'
			},
			{
				plate: 'BCD876',
				modelId: 'Audi A4',
				clientId: null
			}
		]
	}

	function NotImplementedYetException(message){
		this.message = message;
	}

	function notImplemented(functionName){
		var message = "Function " + functionName + " not implemented yet.";
		alert(message);
		throw new NotImplementedYetException(message);
	}

	function fixMe(){
		var message = "You forgot to fix something";
		alert(message);
		throw new NotImplementedYetException(message);
	}

	function KnownException(message) {
		this.message = message;
	}

	function knownExceptionAlert(message) {
		alert(message);
		throw new KnownException(message);
	}

	var connection;
	var dbName;
	var dbVersion;

	databaseObject.connect = function(dbName, dbVersion, onupgradeneededCallback, onsuccessCallback) {
		notImplemented("connect");

		if(connection && dbVersion == connection.version && dbName == connection.name) {
			console.log("You are already connected");
			return;
		}
		
		var request = indexedDB.open(fixMe(), fixMe());
		updateDbNameAndVersion(dbName, dbVersion);		

		request.onerror = function(event) {
			console.log(event);
			knownExceptionAlert('Napotkano błąd podczas połączenia: ' + event.target.error.message);
		};

		request.onsuccess = function(ev) {
			connection = request.result;
			console.log('Połączony do bazy ' + connection.name + ' (ver. ' + connection.version + ')');

			// read more about error handling and error bubbling: https://developer.mozilla.org/pl/docs/IndexedDB/Using_IndexedDB#Handling_Errors
			connection.onerror = function(event){
				console.log(event);
				knownExceptionAlert("Database error: " + event);
			}

			connection.onversionchange = function(event) {
				console.log(event);
				console.log('Pojawiła się nowa wersja! Rozłącz się!');
			};

			if(onsuccessCallback){
				onsuccessCallback(ev);
			}
		};

		request.onblocked = function(event) {
			console.log(event);
			console.log('Połączenie zablokowane. Zresetuj swoje połączenie na innych kartach!');
		};

		request.onupgradeneeded = function(event) {
			if(onupgradeneededCallback){
				connection = request.result;
				onupgradeneededCallback(event);
			}
		}
	};

	function updateDatabaseSchema(updatingSchemaFunction, onsuccessCallback){
		notImplemented('updateDatabaseSchema');

		if(!connection){
			console.log("updateDatabaseSchema expects to have established connection");
			knownExceptionAlert("updateDatabaseSchema expects to have established connection");
		}

		var newVersion = fixMe();
		var name = connection.name;
		connection.close();

		databaseObject.connect(fixMe(), fixMe(), fixMe(), onsuccessCallback);
		console.log(newVersion);
		updateDbNameAndVersion(name, newVersion);
	}

	databaseObject.createDataStoreClients = function(){
		notImplemented('createDataStoreClient');

		connection.createObjectStore(fixMe(), fixMe());
	}

	databaseObject.createDataStoreCarModels = function(){
		notImplemented('createDataStoreCarModels');

		connection.createObjectStore(fixMe(), fixMe());
	}

	databaseObject.createDataStoreCarItems = function(){
		notImplemented('createDataStoreCarItems');

		connection.createObjectStore(fixMe(), fixMe());
	}

	databaseObject.insertClientRows = function(){
		notImplemented('insertClientRows');

		var transaction = connection.transaction(Constants.clients, fixMe());

		var data = dummyData[Constants.clients];
		for(var i = 0; i<data.length; ++i){
			var req = transaction.objectStore(Constants.clients).add(data[i]);

			req.onsuccess = function(){};
			req.onerror = function(){};	
		}
	}

	function generateCarModelId(carModelObject){
		notImplemented('generateCarModelId');
		// carModelObject has properties manufacturer and model
		return fixMe();
	}

	databaseObject.insertCarModelRows = function(){
		notImplemented('insertCarModelRows');

		var transaction = connection.transaction(fixMe(), fixMe());

		var data = dummyData[Constants.models];
		for(var i = 0; i<data.length; ++i){
			var toInsert = data[i];
			toInsert.modelId = generateCarModelId(data[i]);
			var req = transaction.objectStore(Constants.models).add(toInsert);

			req.onsuccess = function(){};
			req.onerror = function(){};	
		}
	}

	databaseObject.insertCarItemRows = function(){
		var transaction = connection.transaction(Constants.items, 'readwrite');

		var data = dummyData[Constants.items];
		for(var i = 0; i<data.length; ++i){
			var req = transaction.objectStore(Constants.items).add(data[i]);

			req.onsuccess = function(){};
			req.onerror = function(){};	
		}
	}

	databaseObject.getClientByPesel = function(pesel){
		notImplemented('getClientByPesel');

		var transaction = connection.transaction(Constants.clients);
		var objectStore = transaction.objectStore(Constants.clients);
		var req = fixMe();

		req.onsuccess = function(event) {
			if(req.result){
				console.log('Successfully fetched user by pesel ' + pesel);
				var user = req.result;
				console.log(user);	
			}else{
				knownExceptionAlert('Cannot fetch user by pesel ' + pesel);
			}
		}
	}

	databaseObject.getAllCarItems = function(){
		notImplemented('getAllCarItems');

		var transaction = connection.transaction(fixMe());
		var objectStore = transaction.objectStore(fixMe());

		var items = [];
		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;
			if (cursor) {
				var key = cursor.key;
				var value = cursor.value;
				items.push({key: key, value: value});
				fixMe();
		  	}else {
		    	console.log(items);
		  	}
		};
	};

	databaseObject.getAllCarItemsCallback = function(onsuccessCallback){
		// notImplemented('getAllCarItems');

		var transaction = connection.transaction(Constants.items);
		var objectStore = transaction.objectStore(Constants.items);

		var items = [];
		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;
			if (cursor) {
				var key = cursor.key;
				var value = cursor.value;
				items.push({key: key, value: value});
		    	cursor.continue();
		  	}else {
		  		if(onsuccessCallback)
		  			onsuccessCallback(items);
		  	}
		};
	}

	databaseObject.getAllCarItemsPromise = function(){
		var p = new promise.Promise();

		var transaction = connection.transaction(Constants.items);
		var objectStore = transaction.objectStore(Constants.items);

		var items = [];
		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;
			if (cursor) {
				var key = cursor.key;
				var value = cursor.value;
				items.push({key: key, value: value});
		    	cursor.continue();
		  	}else {
		  		p.done(null, items);
		  	}
		};

		return p;
	}

	databaseObject.createClientNameIndex = function(){
		createIndex(Constants.clients, 'name', 'name', true);
	}

	function getObjectStore(dataStoreName, mode){
		var transaction;
		if(mode)
			transaction = connection.transaction(dataStoreName, mode);
		else
			transaction = connection.transaction(dataStoreName)

		var objectStore = transaction.objectStore(dataStoreName);
		return objectStore;
	}

	databaseObject.findClient = function(name, onsuccessCallback){
		notImplemented('findClient');

		var transaction = connection.transaction(Constants.clients);
		var objectStore = transaction.objectStore(Constants.clients);
		var index = fixMe();
		var req = index.get(name);

		req.onsuccess = function(event){
			if(req.result){
				console.log('Successfully fetched client with name ' + name);
				console.log(event.target.result);	
				if(onsuccessCallback){
					onsuccessCallback(event.target.result);
				}
			}else{
				console.log('Cannot fetch client with name ' + name);
			}
		}
	};

	databaseObject.createClientIdIndex = function(){
		createIndex(Constants.items, 'clientIdIndex', 'clientId', false);
	};

	function createIndex(objectStoreName, indexName, fieldName, unique){
		notImplemented('createIndex');

		var objectStore = getObjectStore(objectStoreName);
		if(objectStore.indexNames.contains(indexName)){
			console.log("Index" + indexName + " already exists.");
		}else{
			updateDatabaseSchema(function(event){
				var objectStore = event.currentTarget.transaction.objectStore(objectStoreName);
				objectStore.createIndex(fixMe(), fixMe(), fixMe());
				console.log("Index " + indexName + " created successfully.");
			});
		}
	}

	databaseObject.createYearIndex = function(){
		createIndex(Constants.clients, 'yearIndex', 'year', false);
	}

	databaseObject.findCarsForUsers = function(pesel, onsuccessCallback){
		notImplemented('findCarsForUsers');

		var objectStore = getObjectStore(Constants.items);
		var index = objectStore.index('clientIdIndex');

		var singleKeyRange = IDBKeyRange.fixMe();
		var cars = fixMe();

		index.openCursor(singleKeyRange).onsuccess = function(event){
			var cursor = event.target.result;
			if(cursor){
				cars.push(cursor.value);
				cursor.continue();
			}else{
				console.log("There are " + cars.length + " car for user " + pesel);
				console.log(cars);
				if(onsuccessCallback)
					onsuccessCallback(cars);
			}
		}
	};

	databaseObject.clientsJoinCarItems = function(){
		// var clients = databaseObject.getClientsPromise();
		// client.then(function(error, clients){
		// 	var carsByClients = {};
		// 	for(var i=0; i<clients.length; ++i){
		// 		var carsForClient = findCarsForUsersPromise(clients[i].pesel);
		// 		carsByClients.push(carsForClient);
		// 	}
		// };
		// for(var i=0; i<carsByClients.length; ++i){
		// 	carsByClients[i].then(error, cars)
		// }
	}

	databaseObject.findCarsForUsersPromise = function(pesel){
		var p = new promise.Promise();
		var objectStore = getObjectStore(Constants.items);
		var index = objectStore.index('clientIdIndex');

		var singleKeyRange = IDBKeyRange.only(pesel);
		var cars = [];

		index.openCursor(singleKeyRange).onsuccess = function(event){
			var cursor = event.target.result;
			if(cursor){
				cars.push(cursor.value);
				cursor.continue();
			}else{
				console.log("There are " + cars.length + " car for user " + pesel);
				p.done(null, cars);
			}
		}
		return p;
	};

	databaseObject.getClientsPromise = function(){
		var p = new promise.Promise();
		var objectStore = getObjectStore(Constants.clients);

		var clients = [];
		objectStore.openCursor().onsuccess = function(event){
			var cursor = event.target.result;
			if(cursor){
				clients.push(cursor.value);
				cursor.continue();
			}else{
				p.done(null, clients);
			}
		}
		return p;
	};

	databaseObject.findBornBefore = function(year){
		notImplemented("findBornBefore");

		var objectStore = getObjectStore(Constants.clients);
		var index = objectStore.index('yearIndex');
		var range = IDBKeyRange.upperBound(fixMe(), fixMe());

		var users = [];

		index.openCursor(range).onsuccess = function(event){
			var cursor = event.target.result;
			if(cursor){
				fixMe();
				cursor.continue();
			}else{
				console.log(users);
			}
		}
	};

	databaseObject.changeClientYear = function(pesel, newYear){
		notImplemented('changeClientYear');

		var objectStore = getObjectStore(Constants.clients, 'readwrite');
		req = objectStore.get(fixMe());

		req.onsuccess = function(event){
			var client = req.result;
			fixMe();

			var requestUpdate = objectStore.fixMe();
		   	requestUpdate.onsuccess = function(event) {
		    	console.log("Client's year has been updated");
		   	};
		}
	};

	databaseObject.deleteUserByName = function(name, consistencyCheck){
		notImplemented('deleteUserByName');

		databaseObject.findClient(fixMe(), fixMe());

		function deleteUserByPesel(pesel){
			if(consistencyCheck){
				databaseObject.findCarsForUsers(pesel, function(cars){
					if(fixMe()){
						deleteUser(pesel);
					}else{
						console.log("Cannot delete user that has some cars");
					}
				});
			}else{
				deleteUser(pesel);
			}
		}
		
		function deleteUser(pesel){
			var objectStore = getObjectStore(Constants.clients, 'readwrite');
			var req = objectStore.delete(pesel);

			req.onsuccess = function(event){
				console.log("User " + name + " has been deleted.");
			}
		}
	}



	databaseObject.getAllClients = function(callback){
		var transaction = connection.transaction(Constants.clients);
		var objectStore = transaction.objectStore(Constants.clients);

		var items = [];
		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;
			if (cursor) {
				var key = cursor.key;
				var value = cursor.value;
				items.push(value);
				cursor.continue();
		  	}else {
		  		callback(items);
		  	}
		};
	};

	return databaseObject;
})();