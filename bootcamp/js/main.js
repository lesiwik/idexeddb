window.indexedDB = fixMe();
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.mozIDBTransaction || window.msIDBTransaction;
IDBTransaction.READ_WRITE = "readwrite";

function readDBNameAndVersion() {
	if(localStorage.getItem("dbname") !== "undefined")
		document.querySelector("#dbName").value = localStorage.getItem("dbname");
	if(localStorage.getItem("dbversion") !== "undefined")
		document.querySelector("#dbVersion").value = localStorage.getItem("dbversion");
}

function updateDbName(name) {
	localStorage.setItem("dbname", name);
}

function updateDbVersion(version) {
	localStorage.setItem("dbversion", version);	
}

function updateDbNameAndVersion(name, version) {
	updateDbName(name);
	updateDbVersion(version);
}

(function() {
	readDBNameAndVersion();
	dataStore = 'users';
	var dbactions = document.querySelector('#dbactions');
	if (!dbactions) {
		return;
	}

	[].forEach.call(document.querySelectorAll("button[type='button']"), function(element) {
		element.addEventListener('click', function(e) {
			e.preventDefault();
			var action = element.getAttribute('data-action');
			actions.hasOwnProperty(action) && actions[action]();
		}, false);
	});

	
	var actions = {
		'connect': function(){
			dbName = dbactions.querySelector('#dbName').value;
			dbVersion = parseInt(dbactions.querySelector('#dbVersion').value) || 1;
			Database.connect(dbName, dbVersion);
			updateDbNameAndVersion(dbName, dbVersion);
		},

		'create-datastore-clients': Database.createDataStoreClients,
		'create-datastore-car-models': Database.createDataStoreCarModels,
		'create-datastore-car-items': Database.createDataStoreCarItems,
		'insert-client-rows': Database.insertClientRows,
		'insert-car-model-rows': Database.insertCarModelRows,
		'insert-car-item-rows': Database.insertCarItemRows,
		'get-client': function(event){
			var pesel = $('#clientPesel').val();
			Database.getClientByPesel(pesel);
		},

		'refresh-clients': function() {
			Database.getAllClients(function(data) {
				var tbody = document.querySelector('#clients-table tbody');
				var source = $('#table-template').text();
				var template = Handlebars.compile(source);
				var html = template({clients : data});
				tbody.innerHTML = html;
			});
		},

		'get-all-cars': Database.getAllCarItems,

		'get-all-cars-callback': function(){
			
			Database.getAllCarItemsCallback(function(items){
				console.log(items)
			});

		},

		'get-all-cars-promise': function(){
			
			var itemsPromise = Database.getAllCarItemsPromise();
			itemsPromise.then(function(error, items){
				console.log(items);
			});

		},

		'find-client': function(){
			var name = $("#client-name-to-find").val();
			Database.findClient(name);
		},

		'create-client-name-index': Database.createClientNameIndex,
		'create-index-caritems': Database.createClientIdIndex,
		'create-index-year': Database.createYearIndex,
		'find-cars-for-users': function(){
			var pesel = $('#client-id-for-cars-finder').val();
			Database.findCarsForUsers(pesel);
		},

		'find-clients-born-before': function(){
			var year = $('#year').val();
			Database.findBornBefore(year);
		},

		'change-bob-dylan-year': function(){
			var year = $('#bobDylanYear').val();
			Database.changeClientYear(Database.bobDylanPesel, year);
		},

		'delete-client': function(){
			var clientName = $('#client-name-to-delete').val();
			var consistencyCheck = $('#delete-data-consistency').is(':checked');
			Database.deleteUserByName(clientName, consistencyCheck);
		},
	};

})();