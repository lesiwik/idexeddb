window.indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.mozIDBTransaction || window.msIDBTransaction;
IDBTransaction.READ_WRITE = "readwrite";

function readDBNameAndVersion() {
	if(localStorage.getItem("dbname") !== "undefined")
		document.querySelector("#dbName").value = localStorage.getItem("dbname");
	if(localStorage.getItem("dbversion") !== "undefined")
		document.querySelector("#dbVersion").value = localStorage.getItem("dbversion");
}

function updateDbName(name) {
	localStorage.setItem("dbname", name);
}

function updateDbVersion(version) {
	localStorage.setItem("dbversion", version);	
}

function updateDbNameAndVersion(name, version) {
	updateDbName(name);
	updateDbVersion(version);
}

(function() {
	readDBNameAndVersion();
	dataStore = 'users';
	var dbactions = document.querySelector('#dbactions');
	if (!dbactions) {
		return;
	}

	[].forEach.call(document.querySelectorAll("button[type='button']"), function(element) {
		element.addEventListener('click', function(e) {
			e.preventDefault();
			var action = element.getAttribute('data-action');
			actions.hasOwnProperty(action) && actions[action]();
		}, false);
	});

	function connect(callback){
		dbName = dbactions.querySelector('#dbName').value;
		dbVersion = parseInt(dbactions.querySelector('#dbVersion').value) || 1;
		Database.connect(dbName, dbVersion + 1, undefined, callback);
	}
	
	var actions = {
		'connect': function(){
			connect();
		},

		'create-datastore-clients': Database.createDataStoreClients,
		'create-datastore-car-models': Database.createDataStoreCarModels,
		'create-datastore-car-items': Database.createDataStoreCarItems,
		'insert-client-rows': Database.insertClientRows,
		'insert-car-model-rows': Database.insertCarModelRows,
		'insert-car-item-rows': Database.insertCarItemRows,
		'get-client': function(event){
			var pesel = $('#clientPesel').val();
			Database.getClientByPesel(pesel);
		},

		'refresh-clients': function() {
			var data = {clients: [
				{
					pesel: '80050313334',
					name: 'Bob Dylan',
					year: '1941'
				},
				{
					pesel: '90010112345',
					name: 'Elvis Presley',
					year: '1930'
				},
				{
					pesel: '82050313334',
					name: 'Diana Ross',
					year: '1940'
				}
			]};

			var tbody = document.querySelector('#clients-table tbody');
			var source = $('#entry-template').text();
			var template = Handlebars.compile(source);
			var html = template(data);
			tbody.innerHTML = html;
		},

		'get-all-cars': Database.getAllCarItems,

		'find-client': function(){
			var name = $("#client-name-to-find").val();
			Database.findClient(name);
		},

		'create-client-name-index': Database.createClientNameIndex,
		'create-index-caritems': Database.createClientIdIndex,
		'create-index-year': Database.createYearIndex,
		'find-cars-for-users': function(){
			var pesel = $('#client-id-for-cars-finder').val();
			Database.findCarsForUsers(pesel);
		},

		'find-clients-born-before': function(){
			var year = $('#year').val();
			Database.findBornBefore(year);
		},

		'change-bob-dylan-year': function(){
			var year = $('#bobDylanYear').val();
			Database.changeClientYear(Database.bobDylanPesel, year);
		},

		'delete-client': function(){
			var clientName = $('#client-name-to-delete').val();
			var consistencyCheck = $('#delete-data-consistency').is(':checked');
			Database.deleteUserByName(clientName, consistencyCheck);
		},

		'add-city': function(){
			connect(Database.addCity);
		},

		'fetchall': function() {
			var users = connection.transaction([dataStore]).objectStore(dataStore);
			users.openCursor().onsuccess = function(event) {
				var cursor = event.target.result;
				if (cursor) {
					// TODO
				}
			};
		},

		'addrow': function() {
			var email = dbactions.querySelector('#email');
			var name = dbactions.querySelector('#nazwa');
			var text = dbactions.querySelector('#text');

			var req = connection.transaction(dataStore, IDBTransaction.READ_WRITE).objectStore(dataStore).add({
				email: email.value,
				name: name.value,
				text: text.value
			});

			req.onsuccess = function() {
				email.value = '';
				name.value = '';
				text.value = '';
			};

			req.onerror = function(event) {
				var message = event.target.error.message == "Key already exists in the object store." ? 
					"Duplikacja wartości unikalnej (e-mail)" : event.target.error.message;
			}
		},

		'removeall': function() {
			connection.transaction(dataStore, IDBTransaction.READ_WRITE).objectStore(dataStore).clear().onsuccess = function() {
			};
		},

		'closedb': function() {
			if (!connection) {
				return;
			}
			connection && connection.close();
			connection = null;
		},

		'deletedb': function() {
			this.closedb();
			indexedDB.deleteDatabase(dbName);
		}
	};

})();