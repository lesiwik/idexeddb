window.indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.mozIDBTransaction || window.msIDBTransaction;
IDBTransaction.READ_WRITE = "readwrite";
console.log("here54");

(function() {

	[].forEach.call(document.querySelectorAll('form'), function(element) {
		element.addEventListener('submit', function() {
		var action = element.getAttribute('data-action');
		actions.hasOwnProperty(action) && actions[action]();
		}, false);
	});

	[].forEach.call(document.querySelectorAll("button[type='button']"), function(element) {
		element.addEventListener('click', function(event) {
			event.preventDefault();
			var action = element.getAttribute('data-action');
			actions.hasOwnProperty(action) && actions[action]();
		}, false);
	});

	var actions = {
		"login": function(){
			var email = $("#loginEmail").val();
			var password = $("#loginPassword").val();

			tryLogin(email, password,
				function(user){
					loggedUserEmail = user.email;
					$('#logout-button').show();
					redirectTo("showDocuments");
					docCookies.setItem("user", user.email);
				},
				function(){
					$("#loginPassword").val('');
				})

			function tryLogin(email, password, successFn, failFn){
				var t = connection.transaction(usersStore);
				var store = t.objectStore(usersStore);
				store.get(email).onsuccess = function(event){
					if(event.target.result && event.target.result.password === password){
						successFn(event.target.result);
					}else{
						failFn();
					}
				}
			}
		},
		"logout": function(){
			docCookies.setItem('user', '');
		},
		"register": function(){
			var email = $("#registerEmail").val();
			var password = $("#registerPassword").val();
			var repeatPassword = $("#repeatPassword").val();

			tryRegister(email, password, repeatPassword,
				function(){
					redirectTo("login-form");
				},
				function(){
					$("#registerPassword").val('');
					$("#repeatPassword").val('');
				});

			function tryRegister(email, password, repeatPassword, successFn, failFn){ 
				if(password == '' || (password !== repeatPassword))
					failFn();
				else{
					ifEmailNotExists( 
						function(){
							connection.transaction(usersStore, 'readwrite').objectStore(usersStore).add({email: email, password: password}).onsuccess = function(event){
								successFn();
							}
						}, 
						failFn);
				}	

				function ifEmailNotExists(successFn, failFn){
					var t = connection.transaction(usersStore);
					var store = t.objectStore(usersStore);
					store.get(email).onsuccess = function(event){
						if(!event.target.result){
							successFn();
						}else{
							failFn();
						}
					};
				}
			}
		},
		"addDocument": function(){
			var title = $("#title").val();
			var content = $("#content").val();

			saveDocument(title, content,
				function(){
					redirectTo("showDocuments");
				},
				function(error){
					// nie ma sensu robic cos wiecej
					console.log("cos przy zapisywaniu poszlo nie tak")
					console.log(error)
				});

			function saveDocument(title, content, successFn, failFn){ 
				if(title == '' || content == '')
					failFn("blad walidacji");
				else{
					var t = connection.transaction(articlesStore, 'readwrite');
					var store = t.objectStore(articlesStore);
					store.add({ownerEmail: loggedUserEmail, title: title, content: content}).onsuccess = function(event){
						if(event.target.result){
							successFn();
						}else{
							failFn(event.target);
						}
					};
				}
			}		
			
		}
	};

	$("#loginButton").click(function(){
		var login = $.trim($("#modalNickName").text());
		var password = $("#loginPassword").val();
		var user = Database.getByLoginAndPassword(login, password);
		user.then(function(error, res){
			if(error == null && res != null){
				redirectTo('game');
			}
		});
	});

	$("#createNewPlayer").click(function(){
		function registrationAlert(message){
			$('#register-messages').append('<div class="alert alert-warning">' + message + '</div>');
		}

		function registrationSuccessAlert(message){
			$('#register-messages').append('<div class="alert alert-success">' + message + '</div>');	
			$.each($("#register-form").find('input'), function(key, val) {$(val).val('')})
		}

		function clearRegistrationAlert(){
			$('#register-messages').html('');
		}

		function addUser(nickName, password){
			Database.addUser(nickName, password).then(
				function(error, success){
					if(error == null){
						registrationSuccessAlert("Zapisano nowego gracza");
					}else{
						if(error.name == "ConstraintError"){
							registrationAlert('Gracz o podanej nazwie istnieje.');
						}else{
							registrationAlert(error.message);
						}
					}
				}
			);
		}

		clearRegistrationAlert();

		var nickName = $('#nickName').val();
		var password = $('#registerPassword').val();
		var repeatPassword = $('#repeatPassword').val();

		var valid = true;

		if(nickName.length == 0){
			registrationAlert('Nie podałeś nazwy gracza');		
			valid = false;
		}

		if(password.length == 0){
			registrationAlert('Nie podałeś hasła');
			valid = false;
		}

		if(password != repeatPassword){
			registrationAlert('Podane hasła się różnią');
			valid = false;
		}

		if(valid)
			addUser(nickName, password);
	});
})();
