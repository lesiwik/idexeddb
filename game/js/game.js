
	console.log('hi');

	$("#changeToRegisterButton").click(function(){
		redirectTo('register-form');
	});

	$("#changeToLoginButton").click(function(){
		redirectTo('login-form');
	});


	if(1 == 1){
		redirectTo('login-form');
	}

	function redirectTo(subpageId){
		$(".subpage").hide();
		$("input").val('');
		$("#" + subpageId).show();

		if(subpageId === 'login-form'){
			Database.listUsers().then(
				function(error, players){
					if(error == null){
						$('#available-players').html('');
						for(var i = 0; i<players.length; ++i){
							$('#available-players').append('<li><a href="#" data-toggle="modal" data-target="#myModal" class="available-user"><span class="glyphicon glyphicon-user"></span> ' + players[i].nickName + '</a></li>');
						}

						$('.available-user').click(function(){
							$('#modalNickName').html('<span class="glyphicon glyphicon-user"></span> ' + $(this).text());
						});
					}else{
						console.log(error);
					}
				}
			);
		}
	}

	var canvas=document.getElementById("myCanvas");
	var ctx=canvas.getContext("2d");

	var Field = function(_centerX, _centerY, _x, _y, _occupant){
		return {
		    centerX: _centerX,
		    centerY: _centerY,
			x: _x,
			y: _y,
		    occupant: _occupant
		};
	}

	var Board = function(columns, rows, fieldRadius, margin){
		this.fields = [];
		this.rows = rows;
		this.columns = columns;
		this.fieldRadius = fieldRadius;
		this.history = [];
		
		var baseMargin = fieldRadius + margin;
		var space = fieldRadius * 2 + margin;
		
		for(var column = 0; column < columns; ++column){
		    this.fields.push([]);
		    for(var row = 0; row < rows; ++row){
		        var centerX = column * space + baseMargin;
		        var centerY = row * space + baseMargin;
		        drawCircle(centerX, centerY, fieldRadius, ctx, 'white');
		        this.fields[column].push(new Field(centerX, centerY, column, row, null));
		    }    
		}
	}

	Board.prototype.move = function(mousePos){
		var thatBoard = this;
		
		function inside(mousePos, field){
		    var minX = field.centerX - thatBoard.fieldRadius;
		    var maxX = field.centerX + thatBoard.fieldRadius;
		    return mousePos.x >= minX && mousePos.x <= maxX;
		}
		
		function firstAvailableFieldInColumn(column){
		    for(var row = thatBoard.rows - 1; row >= 0; --row){
		        if(thatBoard.fields[column][row].occupant == null)
		            return thatBoard.fields[column][row];
		    }
		    return null;
		}
        
        function getSelectedColumn(mousePos){
            for(var column = 0; column<thatBoard.columns; ++column){
		        if(inside(mousePos, thatBoard.fields[column][0])){
                    return column;
                }
            }
			return null;
        }
        
        function moveForColor(column, color){
            var field = firstAvailableFieldInColumn(column);
            if(field != null){
                drawCircle(field.centerX, field.centerY, thatBoard.fieldRadius, ctx, color);
                field.occupant = color;
                return field;
            }
            return null;
        }
        
        function getRandomAvailableColumn(){
            var randomColumn = getRandomInt(0, thatBoard.columns - 1);
            var available = firstAvailableFieldInColumn(randomColumn) != null;
            if(available)
                return randomColumn;
            
            for(var i=0; i<thatBoard.columns && available == false; ++i){
				if(firstAvailableFieldInColumn(i) != null)
					return i;
            }
            
			return null;
        }

		function isCorrectField(x, y){
			return x >= 0 && x < thatBoard.columns && y >= 0 && y < thatBoard.rows;
		}

		function checkInRow(field, increaseX, increaseY){
			var common = 1;

			for(var x = field.x - increaseX, y = field.y - increaseY; isCorrectField(x, y) && thatBoard.fields[x][y].occupant == field.occupant; x = x - increaseX, y = y - increaseY){
				++common;
			}

			for(var x = field.x + increaseX, y = field.y + increaseY; isCorrectField(x, y) && thatBoard.fields[x][y].occupant == field.occupant; x = x + increaseX, y = y + increaseY){
				++common;
			}

			return common >= 4;
		}

		function areAllOccupied(){
			for(var column = 0; column<thatBoard.columns; ++column)
				for(var row = 0; row<thatBoard.rows; ++row)
					if(thatBoard.fields[column][row].occupant == null)
						return false;

			return true;
		}

		function checkResult(field){
			var options = [{x: 1, y: 0}, {x: 0, y: 1}, {x: 1, y: 1}, {x: 1, y: -1}];

			for(var i = 0; i<options.length; ++i)
				if(checkInRow(field, options[i].x, options[i].y))
					return {finished: true, winner: field.occupant}
			
			if(areAllOccupied())
				return {finished: true, winner: null};

			return {finished: false, winner: null};
		}

		function applyMove(column, color){
			var field = moveForColor(column, color);
			thatBoard.history.push(field);
			return checkResult(field);
		}

		function humanMove(mousePos){
			var selectedColumn = getSelectedColumn(mousePos);
	        if(selectedColumn != null && firstAvailableFieldInColumn(selectedColumn) != null){
				return applyMove(selectedColumn, 'green');
			}
			return null;
		}

		function computerMove(){
			var randomColumn = getRandomAvailableColumn();
			if(randomColumn != null){
		        return applyMove(randomColumn, 'red');
			}
		}

		function formatHistory(){
			return '';
		}
		
		console.log("move");
		
        var result = humanMove(mousePos);			
		if(!result.finished){
			result = computerMove();	
		}

		if(result.finished){
			console.log('koniec:' + result.winner);
			console.log(formatHistory());
		}
            
	}

	var board = new Board(7, 7, 45, 10)

	canvas.addEventListener('click', function(evt) {
		var mousePos = getMousePos(canvas, evt);
		console.log("hello: " + mousePos.x + ' ' + mousePos.y);
		board.move(mousePos);
	}, false);


	function drawCircle(x, y, radius, ctx, color){
		ctx.beginPath();
		ctx.arc(x, y, radius, 0, 2*Math.PI);
		ctx.fillStyle = color;
		ctx.fill();
		ctx.stroke();
	}

	function getMousePos(canvas, evt) {
		var rect = canvas.getBoundingClientRect();
		return {
		    x: evt.clientX - rect.left,
		    y: evt.clientY - rect.top
		};
	}

function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}