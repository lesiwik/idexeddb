var Database = (function(){
	var connection;
	var dbVersion;
	var dbName = "appdb";
	var articlesStore = "articles";
	var playerStore = "players";
	var propertyStore = 'properties';
	var imagesStore = "imagesStore";

	var databaseObject = {
		ready: false,
		playerStore: "players",
		queue: []
	}

	function connect(dbName){
		dbVersion = 7;
		console.log(connection);
		if(connection && dbVersion == connection.version && dbName == connection.name) {
			console.log("Jesteś już połączony");
			return;
		}
		var request = indexedDB.open(dbName, dbVersion);
		

		request.onerror = function(event) {
			console.log(event);
			console.log('Napotkano błąd podczas połączenia: <code>' + event.target.error.message + '</code>', 'danger');
		};

		request.onsuccess = function() {
			connection = request.result;
			console.log('Połączony do bazy <code>' + connection.name + '</code> (ver. ' + connection.version + ')', 'success');
			
			databaseObject.connection = connection;
			databaseObject.ready = true;

			while(databaseObject.queue.length > 0){
				var fn = databaseObject.queue.shift();
				fn.call(databaseObject);
			}

			connection.onversionchange = function(event) {
				console.log(event);
				console.log('Pojawiła się nowa wersja! Rozłącz się!', 'danger');
			};
		};

		request.onblocked = function(event) {
			console.log(event);
			console.log('Połączenie zablokowane. Zresetuj swoje połączenie na innych kartach!', 'danger');
		};

		request.onupgradeneeded = function() {
			connection = request.result;
			if (!connection.objectStoreNames.contains(playerStore)) {
				var users = connection.createObjectStore(playerStore, {
					keyPath: 'nickName'
				});
				console.log('Utworzono data store <b><code>players</code></b> i umieszczono przykładowe dane');
			}

			if (!connection.objectStoreNames.contains(propertyStore)) {
				var users = connection.createObjectStore(playerStore, {
					keyPath: 'key'
				});
				console.log('Utworzono data store <b><code>properties</code></b> i umieszczono przykładowe dane');
			}

			if (!connection.objectStoreNames.contains(articlesStore)) {
				var articles = connection.createObjectStore(articlesStore, {
					autoIncrement: true
				});

				articles.createIndex("title", "title", {unique: true});
				articles.createIndex("ownerEmail", "ownerEmail", {unique: false});

				var req = articles.add({
					ownerEmail: 'bob@dylan.com',
					title: 'Bob Dylan',
					content: 'I am a great musician ;)'
				});
				var req = articles.add({
					ownerEmail: 'bob@dylan.com',
					title: 'Smok wawelski',
					content: 'Smok wawelski gra i spiewa'
				});
				var req = articles.add({
					ownerEmail: 'alice@dylan.com',
					title: 'Reprezentacja Niemiec pokonana',
					content: 'Reprezentacja Niemiec rozgromiona'
				});
				console.log('Utworzono data store <b><code>articles</code></b> i umieszczono przykładowe dane');
			}
		}
	}

	function User(nickName, password){
		return {nickName: nickName, password: password};
	}

	function Property(key, value){
		return {key: key, value: value};
	}

	var stdErrorHandler = function(promise){
		return function(event){
			promise.done(event.target.error, false);
		};
	};

	databaseObject.addDocument = function(storeName, documentObject){
		if(this.ready){
			var p = new promise.Promise();
			var store = this.connection.transaction(storeName, 'readwrite').objectStore(storeName);
			var request = store.add(documentObject);
			request.onsuccess = function(event){
				p.done(null, true);
			};
			request.onerror = function(event){
				p.done(event.target.error, false);
			}
			return p;
		}
	};

	databaseObject.addUser = function(nickName, password){
		return this.addDocument(this.playerStore, User(nickName, password));
	};

	databaseObject.addProperty = function(key, value){
		return this.addDocument(this.propertyStore, Property(nickName, password));
	};

	databaseObject.getByLoginAndPassword = function(login, password){
		if(this.ready){
			var p = new promise.Promise()
			var store = this.connection.transaction(this.playerStore).objectStore(this.playerStore);
			var request = store.get(login);
			request.onsuccess = function(event){
				var res = event.target.result;
				if(res && res.password === password)
					p.done(null, res);
				else
					p.done(null, null);
			};
			request.onerror = stdErrorHandler(p);
			return p;
		}
	}

	databaseObject.listUsers = function(){
		var p = new promise.Promise();

		var listUsersFn = function(){
			var store = this.connection.transaction(this.playerStore).objectStore(this.playerStore);
			var cursorRequest = store.openCursor();
			var users = [];
			cursorRequest.onsuccess = function(event){
				var cursor = event.target.result;

				if(cursor){
					users.push(cursor.value);
					cursor.continue();
				}else{
					p.done(null, users);
				}
			}

			cursorRequest.onerror = stdErrorHandler(p);
		};

		if(this.ready){
			listUsersFn.call(this);
		}else{
			this.queue.push(listUsersFn);
		}

		return p;
	}
	connect("appdb");
	return databaseObject;
})();