// window.indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
window.indexedDB = ? ;
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.mozIDBTransaction || window.msIDBTransaction;
IDBTransaction.READ_WRITE = "readwrite";

(function() {
	dataStore = 'users';
	var dbactions = document.querySelector('#dbactions');
	var logs = document.querySelector('#logs');
	if (!dbactions || !logs) {
		return;
	}

	[].forEach.call(document.querySelectorAll('form'), function(element) {
		element.addEventListener('submit', function() {
		var action = element.getAttribute('data-action');
		actions.hasOwnProperty(action) && actions[action]();
		}, false);
	});

	[].forEach.call(document.querySelectorAll("button[type='button']"), function(element) {
		element.addEventListener('click', function(event) {
			event.preventDefault();
			var action = element.getAttribute('data-action');
			actions.hasOwnProperty(action) && actions[action]();
		}, false);
	});

	var log = function(message, type) {
		logs.innerHTML = '<li class="text-' + (type || 'info') + '">' + message + '</li>' + logs.innerHTML;
	};

	var connection;
	var dbName;
	var dbVersion;
	var actions = {
		'connect': function() {
			dbName = dbactions.querySelector('#dbName').value;
			dbVersion = parseInt(dbactions.querySelector('#dbVersion').value) || 1;
			console.log(connection);
			if(connection && dbVersion == connection.version && dbName == connection.name) {
				log("Jesteś już połączony");
				return;
			}
			var request = indexedDB.open(?, ?);	// nazwa bazy i nazwa i nr wersji
			

			request.onerror = function(event) {
				console.log(event);
				log('Napotkano błąd podczas połączenia: <code>' + event.target.error.message + '</code>', 'danger');
			};

			request.onsuccess = function() {
				connection = request.result;
				log('Połączony do bazy <code>' + connection.name + '</code> (ver. ' + connection.version + ')', 'success');

				connection.onversionchange = function(event) {
					console.log(event);
					log('Pojawiła się nowa wersja! Rozłącz się!', 'danger');
				};
			};

			request.onblocked = function(event) {
				console.log(event);
				log('Połączenie zablokowane. Zresetuj swoje połączenie na innych kartach!', 'danger');
			};

			request.onupgradeneeded = function() {
				connection = request.result;
				if (!connection.objectStoreNames.contains(dataStore)) {
					var users = connection.createObjectStore(dataStore, {
						// keyPath: 'email'
						keyPath: ?
					});
					var req = users.add({
						email: 'bob@dylan.com',
						name: 'Bob Dylan',
						text: 'I am a great musician ;)',
						modelId: generateModelId()
					});
					log('Utworzono data store <b><code>users</code></b> i umieszczono przykładowe dane');
				}
			}
		},

		'fetchall': function() {
			var users = connection.transaction([dataStore]).objectStore(dataStore);
			users.openCursor().onsuccess = function(event) {
				var cursor = event.target.result;
				if (cursor) {
					log(cursor.value.email + ' | ' + cursor.value.name + ' | ' + cursor.value.text);
					cursor.
					continue ();
				}
			};
			log('Pobrano dane', "muted");
		},

		'addrow': function() {
			var email = dbactions.querySelector('#email');
			var name = dbactions.querySelector('#nazwa');
			var text = dbactions.querySelector('#text');

			// var req = connection.transaction(dataStore, IDBTransaction.READ_WRITE).objectStore(dataStore).add({
			// 	email: email.value,
			// 	name: name.value,
			// 	text: text.value
			// });

			var transaction = connection.transaction(<listaDataStore>, <typTranskacji>);
			var objectStore = transaction.objectStore(<dataStore>);
			var request = objectStore.add({
				// filled later by MS/DM
			});

			req.onsuccess = function() {
				// filled later
			};

			req.onerror = function(event) {
				// filled later
			}
		},

		'removeall': function() {
			connection.transaction(dataStore, IDBTransaction.READ_WRITE).objectStore(dataStore).clear().onsuccess = function() {
				log('Usunięto wpisy', "muted");
			};
		},

		'closedb': function() {
			if (!connection) {
				return;
			}
			connection && connection.close();
			connection = null;
			log('Połączenie zamknięte', "danger");
		},

		'deletedb': function() {
			this.closedb();
			indexedDB.deleteDatabase(dbName);
			log('Baza <code>' + dbName + '</code> została usunięta');
		}
	};

})();